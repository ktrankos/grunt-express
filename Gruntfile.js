module.exports = function(grunt) 
{
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      app: {
        src: ['Gruntfile.js', 'client/{,*/}*.js', 'server/{,*/}*.js']
      },
      options: {
        jshintrc: '.jshintrc',
        trailing: false
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'src/<%= pkg.name %>.js',
        dest: 'build/<%= pkg.name %>.min.js'
      }      
    },
    server:{
      config:{
        ip:9001,
        tpl:'swig'
      },
    },
    watch: {      
      scripts: {
        files: ['**/*.js'],
        tasks:['server'],
        options: {
          livereload:{
            port:9000
          }
        },
      },
    }
    
  });


  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-server');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');

  // Default task(s).
  grunt.registerTask('default', ['server', 'watch']);
  grunt.registerTask('hello', 'say hello', function(name){
    if(!name || !name.length)
      grunt.warn('you need to provide a name.');
      console.log('hello ' + name);
  });

};